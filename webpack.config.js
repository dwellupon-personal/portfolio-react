const path = require('path');
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    devServer: {
      historyApiFallback: true,
      hot: true,
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      port: 9090
    },
    output: {
      filename: './public/index.bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    devtool: 'inline-source-map',
    module: {
      rules: [
          {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
                  loader: 'babel-loader'
              }   
          },
          {
              test: /\.s[ac]ss$/i,
              use: [
                // Creates `style` nodes from JS strings
                'style-loader',
                // Translates CSS into CommonJS
                'css-loader',
                // Compiles Sass to CSS
                'sass-loader',
              ],
          },
          { 
            test: /\.(jpe?g|css|gif|png|svg|woff|woff2|eot|svg|ttf|wav|mp3)$/, 
            use: [
              {
                loader: 'file-loader',
              },
            ],
          }
      ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
          patterns: [
            { from: "./src/assets", to: "public" }
          ],
        }),
        new HtmlWebpackPlugin({
          template: './public/index.html'
        }),
        new webpack.ProvidePlugin({
          process: 'process/browser',
        }),
        new Dotenv()
    ]
  }