import React from 'react'; 
import me from '../../assets/imgs/me.png'
  
class About extends React.Component {
    render(){
        return(
            <div className="about">
                <div className="about-content">
                    <div className="content-left">
                        <span>About Me...</span>
                        Tech Enthusiast, Developer, Designer, Avid gamer, father
                        <p>
                            My name is Aaron Wallace. I am a <span>developer</span>, a <span>designer</span>, and a <span>leader</span> of things.
                            Over the last 15 years, I have worked in every step of the design/development pipeline from concept-inception to implementation. This 
                            has included in the trenches and planning the missions.
                        </p>
                        <p>
                            I am a family person first and foremost. I have 5 awesome kids ranging from 15 years old to 4 years old. I am a gamer, and technology enthusiast.
                        </p>
                        <div className="gaming">
                            <a href="https://steamcommunity.com/id/dwellupon/" target="_blank"><i class="fa-brands fa-steam"></i></a>
                            {/* <a href="#" target="_blank"><i class="fa-brands fa-playstation"></i></a> */}
                            <a href="https://live.xbox.com/en-US/Profile?Gamertag=Dwellindel" target="_blank"><i class="fa-brands fa-xbox"></i></a>
                        </div>
                    </div>
                    <div className="content-right">
                        <img src="/public/imgs/me.png" alt=""/>
                    </div>
                </div>
            </div>

        )
    }
}  
  
export default About; 