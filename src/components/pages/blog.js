import React from 'react'; 
import { getPosts } from '../../helpers/api/index';
import Post from '../base/blog/post';
  
class Blog extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            posts: []
        }
    }

    componentDidMount() {
        (async () => { 
            const response = await getPosts();
            let posts = this.state.posts
            for (let x = 0; x < response.data.data.length; x++ ){
                var content = response.data.data[x].post_content.substring(0,340) + '...'
                var date = new Date(response.data.data[x].createdAt);
                posts.push({
                    post_name:response.data.data[x].post_name,
                    post_content:content,
                    createdAt:date.toDateString(),
                    post_slug:'/post/' + response.data.data[x].post_slug
                })
            }
            this.setState({
                posts
            });
        })();  
    }
    
    render(){
        return (
            <div className="blog">
                <div className="blog-content">
                    {this.state.posts.map((post, i) => (
                        <Post key={i} name={post.post_name} content={post.post_content} date={post.createdAt} slug={post.post_slug}></Post>
                    ))}
                </div>
            </div>
        )
    }
} 
  
export default Blog; 