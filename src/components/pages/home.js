import React from 'react'; 
import { Link } from 'react-router-dom';
  
class Home extends React.Component {
    render(){
        return (
            <div className="landing">
                <div className="landing-content">
                    <span>Hi, I'm Aaron Wallace</span>
                    Developer, Web Designer, Aspiring Game Designer
                </div>
                <div className="landing-content-alt">
                    <Link className="btn" to="/about">Learn More About Me?</Link> 
                    <Link className="btn" to="/contact"> Get In Touch?</Link>
                </div>
            </div>
        )
    }
} 
  
export default Home; 