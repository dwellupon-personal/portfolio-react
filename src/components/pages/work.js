import React from 'react'; 
  
class Work extends React.Component {
    render(){
        return (
            <div className="work">
                <span>Work / Experience</span>
                <a target="_blank" href="https://www.linkedin.com/in/dwellupon/" className="btn">Contact Me on LinkedIn to learn more</a>
                <a target="_blank" href="https://www.behance.net/aaronwallace" className="btn">View my design work</a>
                <div className="work-content">
                    <div className="work-col">
                        <div className="wc-title">
                            Bandwaggn
                        </div>
                        <div className="wc-position">
                            Co-founder
                        </div>
                        <p>
                            Bandwaggn allows bands to manage their band, their events, their merch, and their gear all in one place. This was a an idea
                            launched when a fellow musician and friend expressed frustration with the status quo of band management.
                        </p>
                        <div className="skills">
                            Useful Skills:<br /><br />

                            <div className="tag">Team Leadership</div>
                            <div className="tag">Team Management</div>
                            <div className="tag">Cross-functional Team Leadership</div>
                            <div className="tag">Technical Leadership</div>
                            <div className="tag">Management</div>
                            <div className="tag">Mentoring</div>
                            <div className="tag">VUE.js</div>
                            <div className="tag">SCSS</div>
                            <div className="tag">REST</div>
                        </div>
                    </div>
                    <div className="work-col">
                        <div className="wc-title">
                            Bridge Connector
                        </div>
                        <div className="wc-position">
                            Technical Program Manager (latest role)
                        </div>                
                        <p>
                            Bridge Connector focused on full-service, repeatable, healthcare integration solutions. My roles ranged from technical program manager to chief information officer.
                        </p>
                        <div className="skills">
                            Useful Skills:<br /><br />

                            <div className="tag">Team Leadership</div>
                            <div className="tag">Team Management</div>
                            <div className="tag">Cross-functional Team Leadership</div>
                            <div className="tag">Technical Leadership</div>
                            <div className="tag">Management</div>
                            <div className="tag">Mentoring</div>
                            <div className="tag">VUE.js</div>
                            <div className="tag">SCSS</div>
                            <div className="tag">GIT</div>
                            <div className="tag">PHP</div>
                            <div className="tag">REST</div>
                            <div className="tag">SOAP</div>
                            <div className="tag">Laravel</div>
                            <div className="tag">API Integrations</div>
                            <div className="tag">Usability</div>
                            <div className="tag">Agile Methodologies</div>
                            <div className="tag">Healthcare Information Technology</div>
                            <div className="tag">IT Strategy</div>
                            <div className="tag">Vendor Management</div>
                            <div className="tag">IT Management</div>
                            <div className="tag">Strategic Planning</div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}  
export default Work; 