import React from 'react'; 
  
class Contact extends React.Component {
    render(){
        return(
            <div className="contact">
                <div className="contact-content">
                    <div className="content-left">
                        <span>Get In Touch</span>
                        <a target="_blank" href="mailto:dwellupon@gmail.com">
                            dwellupon@gmail.com
                        </a>
                        <a target="_blank" href="mailto:aaron@brokenwiremedia.com">
                            aaron@brokenwiremedia.com
                        </a>
                        <div className="social">
                            <a target="_blank" href="https://www.linkedin.com/in/dwellupon/"><i className="fa-brands fa-linkedin"></i></a>
                            <a target="_blank" href="https://bitbucket.org/dwellupon-personal/"><i className="fa-brands fa-bitbucket"></i></a>
                            <a target="_blank" href="https://angel.co/u/aaron-wallace-dwellupon"><i className="fa-brands fa-angellist"></i></a>
                            <a target="_blank" href="https://twitter.com/dwellupon"><i className="fa-brands fa-twitter"></i></a>
                        </div>
                    </div>
                    <div className="content-right">
                        
                    </div>
                </div>
            </div>
        )
    }
} 
  
export default Contact; 