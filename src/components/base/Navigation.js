import React from "react";
import { Link } from 'react-router-dom'; 

class Navigation extends React.Component {
    render() {
        return (
            <div className="nav">
                <div className="mobile-trigger">
                    <i className="fa-light fa-ellipsis-vertical"></i>
                </div>
                <div className="menu">
                    <Link to="/"><i className="fa-light fa-igloo"></i><span className="hidden">Home</span></Link>
                    <Link to="/about"><i className="fa-light fa-person-from-portal"></i><span className="hidden">About Me</span></Link>
                    <Link to="/blog"><i className="fa-light fa-newspaper"></i><span className="hidden">Blog</span></Link>
                    <Link to="/work"><i className="fa-light fa-images"></i><span className="hidden">Work</span></Link>
                    <Link to="/contact"><i className="fa-light fa-address-card"></i><span className="hidden">Contact</span></Link>
                </div>
                
            </div>
        )    
    }
}

export default Navigation;