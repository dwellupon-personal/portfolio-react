import React from "react";

class Brand extends React.Component {
    render() {
        return (
            <div className="brand">
                <div className="brand-content">
                    <div className="fl-col">
                        <span className="blrg">
                            aw
                        </span>
                    </div>
                    <div className="fl-col">
                        aaron wallace
                        <span className="bsml">
                            leader | developer
                        </span>
                    </div>
                </div>
            </div>
        )    
    }
}

export default Brand;