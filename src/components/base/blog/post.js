import React from 'react'; 
import { Link } from 'react-router-dom'; 
  
class Post extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="blog-post">
                <div className="post-title"><Link to={this.props.slug}>{this.props.name}</Link></div>
                <span className="post-date">{this.props.date}</span>
                <p>{this.props.content}</p>
            </div>
        )
    }
}

export default Post