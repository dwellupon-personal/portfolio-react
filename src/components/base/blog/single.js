import React from 'react'; 
import { getPost } from '../../../helpers/api/index';
  
class PostSingle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: {}
        }
    }

    componentDidMount() {
        (async () => { 
            const response = await getPost(this.props.match.params.slug);
            console.log(response)
            let post = this.state.post;
            var date = new Date(response.data.data.createdAt);
            post = {
                post_name:response.data.data.post_name,
                post_content:response.data.data.post_content,
                createdAt:date.toDateString(),
            };
            this.setState({
                post
            });
        })();  
    }

    render() {
        return(
            <div className="blog">
                <div className="blog-content">
                    <div className="blog-post">
                        <div className="post-title">{this.state.post.post_name}</div>
                        <span className="post-date">{this.state.post.createdAt}</span>
                        <p>{this.state.post.post_content}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default PostSingle