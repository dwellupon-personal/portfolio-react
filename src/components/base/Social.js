import React from "react";

class Social extends React.Component {
    render() {
        return (
            <div className="social">
                <a target="_blank" href="https://www.linkedin.com/in/dwellupon/"><i className="fa-brands fa-linkedin"></i></a>
                <a target="_blank" href="https://www.behance.net/aaronwallace"><i className="fa-brands fa-behance"></i></a>
                <a target="_blank" href="https://bitbucket.org/dwellupon-personal/"><i className="fa-brands fa-bitbucket"></i></a>
                <a target="_blank" href="https://angel.co/u/aaron-wallace-dwellupon"><i className="fa-brands fa-angellist"></i></a>
                <a target="_blank" href="https://twitter.com/dwellupon"><i className="fa-brands fa-twitter"></i></a>
            </div>
        )    
    }
}

export default Social;