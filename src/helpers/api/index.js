import axios from 'axios';

const BaseUrl = process.env.BASE_URL + ":" + process.env.PORT

export async function getPosts (){
    const response = await axios.get(BaseUrl + '/posts')
    return response;
}

export async function getPost (slug){
    const response = await axios.get(BaseUrl + '/posts/' + slug)
    return response;
}