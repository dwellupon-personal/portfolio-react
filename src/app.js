import React from 'react';
import './App.scss';
import './assets/webfonts/fa'
import './assets/webfonts/fa.css'

import Brand from './components/base/Brand';
import Navigation from './components/base/Navigation';
import Social from './components/base/Social';
import Home from './components/pages/home'; 
import About from './components/pages/about'; 
import Blog from './components/pages/blog';  
import Work from './components/pages/work'; 
import Contact from './components/pages/contact'; 
import PostSingle from './components/base/blog/single';
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom'; 

function App() {
  return (
    <Router> 
      <div className="app-content">
        <header>
          <Navigation />
          <Brand />
          <Social />
        </header>
        <Switch> 
          <Route exact path='/' component={Home}></Route> 
          <Route exact path='/about' component={About}></Route> 
          <Route exact path='/blog' component={Blog}></Route>
          <Route path="/post/:slug" component={PostSingle} /> 
          <Route exact path='/work' component={Work}></Route> 
          <Route exact path='/contact' component={Contact}></Route> 
        </Switch> 
      </div>
    </Router> 
  )
}

export default App;